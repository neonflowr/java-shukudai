package shukudai;

import java.lang.Math;

public class QuadraticEq {
	public static void solve(int a, int b, int c) {
		if (a <= 0) {
			System.out.println("a cannot be less or equal to 0");
			return;
		}
		System.out.format("Solving for a = %d, b = %d, c = %d\n", a, b, c);

		double delta = b * b - 4 * a * c;
		if (delta > 0) {
			double root1, root2;
			root1 = (-b + Math.sqrt(delta)) / (2 * a);
			root2 = (-b - Math.sqrt(delta)) / (2 * a);
			System.out.println("x1 = " + root1 + ", x2 = " + root2);
		} else if (delta == 0) {
			double root = -b / (2 * a);
			System.out.println("x1 = x2 = " + root);
		} else {
			System.out.println("Root is complex");
		}
	}
}


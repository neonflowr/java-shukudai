package shukudai;

public class DaysToClimb {
	public static int calc(int height, int climb_dist, int backoff, int days) {
		if (height <= 0 || climb_dist < backoff) {
			System.out.println("Invalid input\n");
			return -1;
		}
		while(true) {
			height -= climb_dist;
			if (height <= 0)
				return days;
			height += backoff;
			days++;
		}
	}
}

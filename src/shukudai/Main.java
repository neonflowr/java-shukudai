package shukudai;

public class Main {

    public static void main(String[] args) {
	// 三角
	{
	    int h = 15;
	    TrianglePrint.print(h);
	}
	/////////////////

	// Pascal's triangle
	{
	    int h = 10;
	    TrianglePrint.pascal_triangle(h);
	}
	/////////////////

	// e
	{
	    int n = 50000;
	    EulersNumber.calc(n);
	}
	//////////////////////

	// Binary representation
	{
	    int num = 0xFFFF2;
	    BinaryRep.get_rep(num);
	}
	//////////////////////////////////

	// Quadratic Equation
	{
	    int a, b, c;
	    a = 3;
	    b = -5;
	    c = 2;
	    QuadraticEq.solve(a, b, c);
	}
	///////////////////////////////////

	// Days to cover distance
	{
	    int height, speed, backoff, days;
	    height = 20;
	    speed = 7;
	    backoff = 3;
	    days = 1; // Might need to re-check exactly how to calculate this
	    days = DaysToClimb.calc(height, speed, backoff, days);
	    System.out.format("With height=%d, speed=%d, backoff=%d, days to cover the distance is: %d\n", height, speed, backoff,
			      days);
	}
	////////////

    }

}

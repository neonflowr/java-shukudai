package shukudai;

public class TrianglePrint {
    public static int level_count(int x) {
	return 1 + 2 * x;
    }

    public static void print_spaces(int count) {
	for(int i = 0; i < count; i++){
	    System.out.print(" ");
	}
    }

    public static void print_triangle_character(int count) {
	for(int i = 0; i < count; i++){
	    System.out.print("*");
	}
    }

    public static void print_line(int i, int max_count) {
	System.out.format("%d\t", i+1);
	int current     = level_count(i);
	int space_count = (max_count - current) / 2;
	print_spaces(space_count);
	print_triangle_character(current);
	print_spaces(space_count);
	System.out.print("\n");
    }

    public static int factorial(int n) {
	int prod = 1;
	while(n > 0) {
	    prod *= n--;
	}
	return prod;
    }
    

    public static int binomial_coefficient(int n, int k) {
	return factorial(n) / (factorial(k) * factorial(n - k));
    }

    
    public static void pascal_triangle(int h) {
	int i, j;
	for(i = 0; i < h; i++) {
	    for(j = 0; j <= i; j++) {
		System.out.print(binomial_coefficient(i, j) + " "); //動的計画法の方がいいはずだ
	    }
	    System.out.print("\n");
	}
    }

	
    public static void print(int h) {
	int max_count = TrianglePrint.level_count(h-1);
	int i = 0;
	int limit = h - 5;
	for(; i < limit; i += 5) {
	    TrianglePrint.print_line(i, max_count);
	    TrianglePrint.print_line(i+1, max_count);
	    TrianglePrint.print_line(i+2, max_count);
	    TrianglePrint.print_line(i+3, max_count);
	    TrianglePrint.print_line(i+4, max_count);
	}
	for(; i < h; i++) {
	    TrianglePrint.print_line(i, max_count);
	}
	System.out.println("Unrolled print");
    }

    public static void print_lin(int h) {
	int max_count = TrianglePrint.level_count(h-1);
	for(int i = 0; i < h; i++) {
	    TrianglePrint.print_line(i, max_count);
	}
	System.out.println("Linear print");
    }
}


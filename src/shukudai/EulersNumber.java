package shukudai;

public class EulersNumber {
	public static double calc(int n) {
		double result = 1;
		double ratio = n;
		int limit = n - 5;
		int i;
		for(i = 0; i < limit; i += 5) {
			result = result * (1 + (1 / ratio)) * (1 + (1 / ratio)) * (1 + (1 / ratio)) * (1 + (1 / ratio)) * (1 + (1 / ratio));
		}
		for(; i < n; i++) {
			result *= 1 + (1 / ratio);
		}

		System.out.println("[UNROLLED] Euler's for n = " + n + ": " + result);
		return result;
	}

	public static double calc_lin(int n) {
		double result = 1;
		double ratio = n;
		int i;
		for(i = 0; i < n; i++) {
		    result *= 1 + (1 / ratio);
		}
		System.out.println("[LINEAR] Euler's for n = " + n + ": " + result);
                return result;
	}
}

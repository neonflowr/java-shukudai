package shukudai;

import java.util.Stack;

public class BinaryRep {
    public static void get_rep(int num) {
        int remainder, quo;
        quo = num;
        Stack<Integer> s = new Stack<Integer>();
        while(quo > 0) {
            remainder = quo % 2;
            quo = quo / 2;
            s.push(remainder);
        }
        System.out.print("Binary representation for n = " + num + ": ");
        while(!s.empty()) {
            System.out.print(s.pop());
        }
        System.out.print("\n");
    }
}
